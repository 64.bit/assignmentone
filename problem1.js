const problem1=(inventory)=>{
  //if inventory is not valid
    if(!inventory || !Array.isArray(inventory) || !inventory.length)
      return 'Invalid Argument!';
    
    // for storing details of car-33
    let car33;

    for(let index=0;index<inventory.length;index++){

        if(inventory[index].id===33) {

            car33=inventory[index];

            break; //if car-33 found then no need to proceed further
        }
    }
       return car33;
}
module.exports=problem1;