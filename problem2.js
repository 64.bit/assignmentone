
const problem2=(inventory)=>{

  //if inventory is not valid
  if(!inventory || !Array.isArray(inventory) || !inventory.length)
  return 'Invalid arguments!'
  
  //storing last car of the inventory
  const lastCar= inventory[inventory.length-1];
  
  return lastCar;
}
module.exports=problem2;