function customComparator(firstCar,secondCar){

    const firstCarModel=firstCar.car_model.toUpperCase();
    
    const secondCarModel=secondCar.car_model.toUpperCase();

    if(firstCarModel<secondCarModel)

    return -1;

    else if(firstCarModel>secondCarModel)

    return 1;

    else 

    return 0;
}
const problem3=(inventory)=>{
  
  //if inventory is not valid
   if(!inventory || !Array.isArray(inventory) || !inventory.length)
   return 'Invalid Argument!'

   //sorting on basis of custom comparator
   inventory.sort(customComparator);

   //for storing model of the car
   let modelNames=[];

   for(let index=0;index<inventory.length;index++)
     modelNames.push(inventory[index].car_model);

   return modelNames;
}
module.exports=problem3;