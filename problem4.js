const problem4=(inventory)=>{

    //if inventory is not valid
  if(!inventory || !Array.isArray(inventory) || !inventory.length)
  return 'Invalid Argument!';

  //for storing car_year
  let yearList=[];

  for(let index=0;index<inventory.length;index++) 
    yearList.push(inventory[index].car_year);
    
  return yearList;
}
module.exports=problem4;