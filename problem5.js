const problem5=(inventory)=>{

    //if inventory is not valid
  if(!inventory || !Array.isArray(inventory) || !inventory.length)
  return 'Invalid Argument!'

  // for storing cars older than year 2000
  let olderCars=[]

  for(let index=0;index<inventory.length;index++){

      if(inventory[index].car_year<2000) 
        olderCars.push(inventory[index].car_year);

  }
  
  return olderCars;
}
module.exports=problem5;