const problem6=(inventory)=>{

    //if inventory is not valid
  if(!inventory || !Array.isArray(inventory) || !inventory.length)
  return 'Invalid Argument!';

  //for storing bmw as well as audi cars
  let bmwAndAudi=[];

  for(let index=0;index<inventory.length;index++){

      if((inventory[index].car_make==='BMW' )|| (inventory[index].car_make==='Audi'))
          bmwAndAudi.push(inventory[index]);
  }
  
  return bmwAndAudi;
}
module.exports=problem6;