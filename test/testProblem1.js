const problem1=require('../problem1');

const inventory=require('../data');

const testProblem1=()=>{
  const validTest=problem1(inventory);

  //invalid argument
  const invalidTest1=problem1();

  const invalidTest2=problem1([]);

  const invalidTest3=problem1(1);
  
  console.log(validTest,invalidTest1,invalidTest2,invalidTest3);
}
testProblem1();
