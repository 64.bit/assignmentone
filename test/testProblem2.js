const inventory=require('../data');

const problem2=require('../problem2');

const testProblem2=()=>{

    const validTest=problem2(inventory);

    //invalid argument
    const invalidTest1=problem2();

    const invalidTest2=problem2([]);

    const invalidTest3=problem2(1);
    
    console.log(validTest,invalidTest1,invalidTest2,invalidTest3);
}
testProblem2();