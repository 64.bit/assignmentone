const inventory=require('../data');

const problem3=require('../problem3');

const testProblem3=()=>{

  const validTest=problem3(inventory);

  const invalidTest1=problem3([]);

  const invalidTest2=problem3();

  const invalidTest3=problem3(22);
  
  console.log(validTest,invalidTest1,invalidTest2,invalidTest3);
}

testProblem3();