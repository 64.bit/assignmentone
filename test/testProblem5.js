const inventory=require('../data');

const problem5=require('../problem5');

const testProblem5=()=>{

    const validTest=problem5(inventory);

    const invalidTest1=problem5([]);

    const invalidTest2=problem5();

    const invalidTest3=problem5(22);
    
    console.log(validTest,invalidTest1,invalidTest2,invalidTest3);
}

testProblem5();