const inventory=require('../data');
const problem6=require('../problem6');
const testProblem6=()=>{
    
    const validTest=problem6(inventory);

    const invalidTest1=problem6([]);

    const invalidTest2=problem6();

    const invalidTest3=problem6(22);

    console.log(validTest,invalidTest1,invalidTest2,invalidTest3);
}
testProblem6();